FROM ubuntu:18.04

LABEL maintainer="Luiz Schmitt <lzschmitt@gmail.com>"

ARG DEBIAN_FRONTEND=noninteractive

ENV LC_ALL=C.UTF-8 \
    TIMEZONE=America/Manaus

RUN echo $TIMEZONE | tee /etc/timezone

RUN apt-get update && apt-get install php-cli php-pear php-curl php-gd php-json php-mbstring php-mysql php-xml php-zip php-dev libaio1 curl wget alien make git unzip php-sqlite* ca-certificates --no-install-recommends -qqy

RUN curl -sS https://getcomposer.org/installer | php -- \
    --install-dir=/usr/local/bin \
    --filename=composer

RUN curl -sL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt-get install -y nodejs

RUN apt-get clean autoclean && apt-get autoremove --yes &&  rm -rf /var/lib/{apt,dpkg,cache,log}/

RUN mkdir /opt/oracle \
    && mkdir /opt/php-src \
    && mkdir /opt/oci8

ADD ./php-7.2.24-src.pack /opt/php-src
ADD ./oci8-2.2.0.pack /opt/oci8
ADD ./instantclient-basic-linux.x64-12.2.0.1.0.pack /opt/oracle/instantclient-basic-linux.x64-12.2.0.1.0.pack
ADD ./instantclient-sdk-linux.x64-12.2.0.1.0.pack /opt/oracle/instantclient-sdk-linux.x64-12.2.0.1.0.pack

RUN unzip /opt/oracle/instantclient-basic-linux.x64-12.2.0.1.0.pack -d /opt/oracle \
    && unzip /opt/oracle/instantclient-sdk-linux.x64-12.2.0.1.0.pack -d /opt/oracle \
    && ln -s /opt/oracle/instantclient_12_2/libclntsh.so.12.1 /opt/oracle/instantclient_12_2/libclntsh.so \
    && ln -s /opt/oracle/instantclient_12_2/libclntshcore.so.12.1 /opt/oracle/instantclient_12_2/libclntshcore.so \
    && ln -s /opt/oracle/instantclient_12_2/libocci.so.12.1 /opt/oracle/instantclient_12_2/libocci.so \
    && rm -rf /opt/oracle/*.pack

ENV LD_LIBRARY_PATH  /opt/oracle/instantclient_12_2:${LD_LIBRARY_PATH}

RUN unzip /opt/oci8/oci8-2.2.0.pack -d /opt/oci8 \
    && cd /opt/oci8/oci8-2.2.0 \
    && phpize \
    && ./configure --with-oci8=instantclient,/opt/oracle/instantclient_12_2 \
    && make && make install \
    && rm -rf /opt/oci8/*.pack

RUN unzip /opt/php-src/php-7.2.24-src.pack -d /opt/php-src \
    && cd /opt/php-src/ext/pdo_oci \
    && phpize \
    && ./configure --with-pdo-oci=instantclient,/opt/oracle/instantclient_12_2,12.2 \
    && make && make install \
    && rm -rf /opt/php-src/*.pack

RUN echo "extension=oci8.so" > /etc/php/7.2/mods-available/oci8.ini \
    && echo "extension=pdo_oci.so" > /etc/php/7.2/mods-available/pdo_oci.ini \
    && phpenmod oci8 && phpenmod pdo_oci

ENV NLS_TERRITORY=BRAZIL \
	NLS_LANG="BRAZILIAN PORTUGUESE_BRAZIL.UTF8" \
	NLS_LANGUAGE="BRAZILIAN PORTUGUESE" \
	NLS_CHARACTERSET=UTF8 \
	NLS_NCHAR_CHARACTERSET=AL32UTF8 \
	NLS_CURRENCY="R$" \
	NLS_NUMERIC_CHARACTERS=".," \
	NLS_SORT=WEST_EUROPEAN_AI \
	NLS_COMP=BINARY \
	NLS_DATE_FORMAT="RRRR-MM-DD HH24:MI:SS" \
	NLS_TIMESTAMP_FORMAT="RRRR-MM-DD HH24:MI:SS.FF"