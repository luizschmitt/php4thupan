# Ubuntu 18.04

Container criado para rodar exclusivamente o Thupan Framework

## Configuração do container

- Ubuntu 18.04 LTS
- PHP 7.2.24
  - OCI8 habilitado
  - PDO_OCI habilitado
- Composer
- NodeJS 12.x
- Oracle Instant Client 12.2